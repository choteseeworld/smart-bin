//************************************Constant pin***************************************
const int motorA = D8;
const int motorB = D1;
const int sensorPin = D6;
const int switchPin = D7;
const int modeAutoPin = D5;
const int modeManPin = D2;
const int stopPinO = D3;
const int stopPinC = D4;
//const int btnClose = D3;
//const int btnOpen = D4;
//**********************************END constant pin**************************************


//************************************Boolean flag interrupt******************************
bool flagSensor = false;
bool flagSwitch = false;
bool stopper = false;
//bool openCount = false;
//bool closeCount = false;
bool sw3sec = false;
bool timer = false;
int flagAuto;    //flag for auto sensor
int flagMan;     //flag for manual switch
//**********************************END Boolean flag interrupt****************************
unsigned long Time;

void setup() {
  pinMode(motorA, OUTPUT);
  pinMode(motorB, OUTPUT);
  pinMode(sensorPin, INPUT_PULLUP);
  pinMode(switchPin, INPUT_PULLUP);
  pinMode(modeAutoPin, INPUT_PULLUP);
  pinMode(modeManPin, INPUT_PULLUP);
  pinMode(stopPinC, INPUT_PULLUP);
  pinMode(stopPinO, INPUT_PULLUP);
  //  pinMode(btnOpen, INPUT_PULLUP);
  //  pinMode(btnClose, INPUT_PULLUP);

  attachInterrupt(digitalPinToInterrupt(sensorPin), sensor_ISR, FALLING);     //interrupt when sensor active.
  attachInterrupt(digitalPinToInterrupt(switchPin), switch_ISR, FALLING);     //interrupt when press switch.
  attachInterrupt(digitalPinToInterrupt(stopPinO), stop_ISR, FALLING);
  attachInterrupt(digitalPinToInterrupt(stopPinC), stop_ISR2, FALLING);
  //  attachInterrupt(digitalPinToInterrupt(btnClose), close_ISR, FALLING);
  //  attachInterrupt(digitalPinToInterrupt(btnOpen), open_ISR, RISING);

  Serial.begin(115200);
  //  Serial.setTimeout(2000);
  //
  //  while (!Serial) { }
  //
  //  Serial.print("I'm awake, but I'm going into deep sleep mode until RESET pin is connected to a LOW signal");
  //  ESP.deepSleep(0);

}


void loop() {
  flagAuto = digitalRead(modeAutoPin);
  flagMan = digitalRead(modeManPin);
  if(digitalRead(stopPinO)== 0){
    Serial.println("press");
  }
  //  Serial.print(flagAuto);
  //  Serial.print("    ");
  //  Serial.println(flagAuto);

  //  if (closeCount == true) {
  //    motorReverse();
  //    delay(1600);
  //    motorStop();
  //    closeCount = false;
  //    openCount = false;
  //  }
  //  if (openCount == true) {
  //    motorAction();
  //    delay(1600);
  //    motorStop();
  //    closeCount = false;
  //    openCount = false;
  //  }
  if (flagMan == 1 && sw3sec == true){
    sw3sec = false;
      motorReverse();
      delay(1800);
      motorStop();
      timer = false;
      flagSwitch = false;
  }
  if (flagAuto == 0) {
    if (flagSensor == true) {
      if (digitalRead(sensorPin) == 0) {
        detachInterrupt(digitalPinToInterrupt(stopPinO));
        motorAction();
        delay(1800);
        motorStop();
        delay(3000);
      }
      if (digitalRead(sensorPin) == 1) {
//        Serial.println(digitalRead(sensorPin));
        motorReverse();
        delay(1800);
        motorStop();
//        flagSensor = false;
      }
    }
  }

  if (flagMan == 0) {
    if(flagSwitch == true){
    if (flagSwitch == true && sw3sec == true){
      sw3sec = false;
      motorReverse();
      delay(2000);
      motorStop();
      timer = false;
      flagSwitch = false;
    }
    if (flagSwitch == true && timer == false){
      timer = true;
     Time = millis();
     Serial.print("Time ");
      Serial.println(Time);
    }
    if(digitalRead(switchPin) == 0 && millis() - Time < 3000){
      Serial.print(digitalRead(switchPin));
      Serial.println(Time);
      return;
    }
//    if(digitalRead(switchPin) == 1 ){
//      Serial.println("Time Reset");
//       Time = millis();
//    }
    if(millis() - Time >= 3000 && flagSwitch == true){
      
      Serial.println(Time);
      Serial.println("Always open");
      sw3sec = true;
      motorAction();
      delay(1800);
      motorStop();
      flagSwitch = false;    
    }
  
    if (flagSwitch == true) {
      Serial.println("normal open");
      stopper == false;
      motorAction();
      delay(1800);
      motorStop();
      delay(3000);
      motorReverse();
      delay(2200);
      motorStop();
      timer = false;
      flagSwitch = false;
    }}
  }

  motorStop();
  flagSensor = false;
  flagSwitch = false;
}


void sensor_ISR() {
  flagSensor = true;
}


void switch_ISR() {
  flagSwitch = true;
}

//void close_ISR() {
//  //  closeCount = true;
//  motorReverse();
//  delay(1600);
//  motorStop();
//}
//
//void open_ISR() {
//  //  openCount = true;
//  motorAction();
//  delay(1600);
//  motorStop();
//}

void stop_ISR() {
  motorStop();
  flagSensor = false;
  flagSwitch = false;
//  stopper = true;
}
void stop_ISR2() {
  motorStop();
  flagSensor = false;
  flagSwitch = false;
//  detachInterrupt(digitalPinToInterrupt(stopPinC));

//  stopper = true;
}

void motorAction() {
  //  Serial.println("motorAction");
  detachInterrupt(digitalPinToInterrupt(stopPinC));
  digitalWrite(motorA, HIGH);
  digitalWrite(motorB, LOW);
  attachInterrupt(digitalPinToInterrupt(stopPinO), stop_ISR, FALLING);
}


void motorStop() {
  //  Serial.println("motorStop");
  digitalWrite(motorA, LOW);
  digitalWrite(motorB, LOW);
}


void motorReverse() {
  //  Serial.println("motorReverse");
  detachInterrupt(digitalPinToInterrupt(stopPinO));
  attachInterrupt(digitalPinToInterrupt(stopPinC), stop_ISR2, FALLING);
  digitalWrite(motorA, LOW);
  digitalWrite(motorB, HIGH);
//  attachInterrupt(digitalPinToInterrupt(stopPinC), stop_ISR2, FALLING);
//  detachInterrupt(digitalPinToInterrupt(stopPinO));
}
